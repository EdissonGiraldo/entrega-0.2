package Ejercicios;

//importamos el metodo swing para ingresar los datos por el teclado
import javax.swing.JOptionPane;

public class P7_Primalidad {

	public static void main(String[] args) {
		// Solicitamos los datos mediante la varible y Metodo Jframe
		int num = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un numero: "));
		// Creamos una segunda variable que nos ayuda a verificar si el numero es primo
		// o no

		int num2 = num - 1;
		// la igualamos a num y le restamos 1 apartir del numero anterior, esto porque
		// todos los numeros son divisibles entre si mismo.

		// con el ciclo while verificamos que el resto de la division entre los numeros
		// sea diferente de 0
		while ((num % num2) != 0) {
			num2--;
		}

		// Utilizamos los condicionales para saber si el numero es primo o no y
		// mostramos el mensaje en pantalla
		if (num2 == 1) {
			JOptionPane.showMessageDialog(null, "El numero " + num + " es primo");
		} else {
			JOptionPane.showMessageDialog(null, "El numero " + num + " no es primo");
		}

	}

}
