package Ejercicios;

// importamos la libreria scanner para pedir los datos del usuario
import java.util.Scanner;

public class P8_Conjetura_Goldbach {

	public static void main(String[] args) {
//Mostramos mensajes de bienvenida al usuario
		System.out.println("Sistema Inteligente de Busqueda Golbach - SIBG");
		System.out.println(" ");
		// declaramos las variables
		int number = 0;
		int[] matriz;
		int result = 0;

		boolean esPrimo = true;
// utlizamos el ciclo do while para inicializar el procedimiento
		do {

			@SuppressWarnings("resource")
			Scanner numero = new Scanner(System.in);

			System.out.println("por favor ingrese un n�mero par mayor que dos");

			int a = numero.nextInt();
			number = a;

		} while (number % 2 != 0 || number <= 2);

		System.out.println("Ejecutando... por favor espere");

		matriz = new int[number];
		int b = 0;
// utlizmos el cilo for para incontrar el for
		for (int i = 0; i <= number; i++) {

			esPrimo = encontrarPrimo(i);

			if (esPrimo == true) {

				matriz[b] = i;
				b++;

			}
		}
// una vez reliado los calculos utiliamos los ciclos for para mostrar en resultado final al usuario.
		System.out.println("Su resultado es: ");
		for (int i = 0; i < matriz.length; i++) {

			for (int j = i; j < matriz.length; j++) {

				if (matriz[i] + matriz[j] == number) {

					System.out.println(matriz[i] + " + " + matriz[j] + " = " + number);
					result++;

				}

			}
		}
		System.out.println(result + " Resultados encontrados");

	}

// hacemos uso del metodo bolean.
	public static boolean encontrarPrimo(int a) {

		int contador = 2;
		boolean primo = true;

		while ((primo) && (contador != a)) {
			if (a % contador == 0)
				primo = false;
			contador++;
		}
		return primo;
	}
}