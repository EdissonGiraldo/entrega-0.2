package Ejercicios;

//importamos el libreria swing para ingresar los datos por el teclado
import javax.swing.JOptionPane;

public class P4_Juego_Dardos {

	public static void main(String[] args) {
		// Solicitamos el numero mediante la varible y la clase  Jframe
		float x = Float.parseFloat(JOptionPane.showInputDialog("Ingrese el eje x"));
		// los pedimos de tipo float y hacemos la conversion para que JOption puedan
		// recibir los datos.
		float y = Float.parseFloat(JOptionPane.showInputDialog("Ingrese el eje y"));

		//Hacemos uso de las funcion Math.
		double distancia = Math.sqrt((x * x) + (y * y));

		// Utilizamos los condicionales para mostrar el resultado que haya obtenido el
		// dardo.
		if (distancia < 1) {
			// Mostramos el resultado en pantalla emergenteF
			JOptionPane.showMessageDialog(null, "El puntaje es de 15");
		} else {
			if (1 > distancia || distancia <= 2) {
				// Mostramos el resultado en pantalla emergente
				JOptionPane.showMessageDialog(null, "El puntaje es de 9");
			} else {
				if (2 > distancia || distancia <= 3) {
					// Mostramos el resultado en pantalla emergente
					JOptionPane.showMessageDialog(null, "El puntaje es de 5");
				} else {
					if (3 > distancia || distancia <= 4) {
						// Mostramos el resultado en pantalla emergente
						JOptionPane.showMessageDialog(null, "El puntaje es de 2");
					} else {
						if (4 < distancia || distancia <= 5) {
							// Mostramos el resultado en pantalla emergente
							JOptionPane.showMessageDialog(null, "El puntaje es de 1");
						} else {
							if (distancia > 5) {
								// Mostramos el resultado en pantalla emergente
								JOptionPane.showMessageDialog(null, "El puntaje es de 0");
							}
						}
					}
				}
			}
		}

	}
}