package Ejercicios;

//importamos el libreria swing para ingresar los datos por el teclado
import javax.swing.JOptionPane;

public class P2_Par_Impar {

	public static void main(String[] args) {
		// Solicitamos el numero mediante la varible y la clase  Jframe
		int Num = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un numero entero: "));

		// Utilizamos el condicional para saber si el numero es par o impar

		if (Num % 2 == 0) {
			JOptionPane.showMessageDialog(null, "El numero " + Num + " es par ");
			// Encontramos el numero par y lo mostramos en pantalla
		} else {
			JOptionPane.showMessageDialog(null, "El numero " + Num + " es impar ");
			// Encontramos el numero impar y lo mostramos en pantalla
		}
	}
}
 	