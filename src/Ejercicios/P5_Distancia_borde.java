package Ejercicios;

import java.util.Scanner;

public class P5_Distancia_borde {
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner datos = new Scanner(System.in);
		// creamos las variables nesesarias para el ejercicio
		float a, b, x, y;
		// pedimos a el usuario las medidas de el rectangulo inicial
		System.out.println("Ingresa los datos de medidas de rectangulo A,B");
		a = datos.nextFloat();
		b = datos.nextFloat();
		// pedimos a el usuario la posicion en x
		System.out.println("Ingresa los datos de tu posicion X dentro de el rectangulo");
		x = datos.nextFloat();

		// evaluamos que la posicion en x esta dentro de el rectangulo si no albertimos
		// y terminamos el programa,
		if (x > a) {
			System.out.println("tu posicion en x, esta fuera de el rectangulo");
		} // si esta dentro de el rectangulo le pedimos su posicion en y,
		else {
			System.out.println("Ingresa los datos de tu posicion y dentro de el rectangulo");
			y = datos.nextFloat();
			// evaluamos si y esta dentro de el rectangulo si no albertimos y terminamosd el
			// programa,
			if (y > b) {
				System.out.println("tu posicion en y, esta fuera de el rectangulo");
			} else {
				// creamos las variable que contendran las distancias de la posicion x,y a las
				// fronteras,
				float fron1, fron2, fron3, fron4;
				fron1 = b - y;
				fron2 = y;
				fron3 = x;
				fron4 = a - x;
				// evaluamos cual es la menor distancia hacia las fronteras,
				if (fron1 < fron2) {
					if (fron1 < fron3) {
						if (fron1 < fron4) {
							System.out.println("la fontera mas cercana es la 1");
						}
					}
				}
				if (fron2 < fron1) {
					if (fron2 < fron3) {
						if (fron2 < fron4) {
							System.out.println("la fontera mas cercana es la 2");
						}
					}
				}
				if (fron3 < fron1) {
					if (fron3 < fron2) {
						if (fron3 < fron4) {
							System.out.println("la fontera mas cercana es la 3");
						}
					}
				}
				if (fron4 < fron1) {
					if (fron4 < fron2) {
						if (fron4 < fron3) {
							System.out.println("la fontera mas cercana es la 4");
						}
					}
				}
				// evalua la igualdad de 1 y2,
				if (fron1 == fron2) {
					if (fron1 < fron3) {
						if (fron1 < fron4) {
							System.out.println("pudes decidir las fonteras 1 y 2 estan a la misma distancia");
						}
					}
				}
				// evalua la igualdad de 1 y 3
				if (fron1 == fron3) {
					if (fron1 < fron2) {
						if (fron1 < fron4) {
							System.out.println("pudes decidir las fonteras 1 y 3 estan a la misma distancia");
						}
					}
				}
				// evalua la igualdad de 1 y4
				if (fron1 == fron4) {
					if (fron1 < fron2) {
						if (fron1 < fron3) {
							System.out.println("pudes decidir las fonteras 1 y 4 estan a la misma distancia");
						}
					}
				}
				// evalua la igualdad de 2 y 3,
				if (fron2 == fron3) {
					if (fron2 < fron1) {
						if (fron2 < fron4) {
							System.out.println("pudes decidir las fonteras 1 y 3 estan a la misma distancia");
						}
					}
				}
				// evalua la igualdad entre 2 y 4
				if (fron2 == fron4) {
					if (fron2 < fron1) {
						if (fron2 < fron3) {
							System.out.println("pudes decidir las fonteras 2 y 4 estan a la misma distancia");
						}
					}
				}
				// evalua la igualdad 3 y 4
				if (fron3 == fron4) {
					if (fron4 < fron1) {
						if (fron4 < fron2) {
							System.out.println("pudes decidir las fonteras 4 y 3 estan a la misma distancia");
						}
					}
				}

			}

		}
	}
}