package Ejercicios;

//importamos el libreria swing para ingresar los datos por el teclado
import javax.swing.JOptionPane;

public class P3_Tipo_Triangulo {

	public static void main(String[] args) {
		// Solicitamos el numero mediante la varible y la clase  Jframe
		float a = Float.parseFloat(JOptionPane.showInputDialog("Ingrese el lado a del triangulo : "));
		float b = Float.parseFloat(JOptionPane.showInputDialog("Ingrese el lado b del triangulo : "));
		float c = Float.parseFloat(JOptionPane.showInputDialog("Ingrese el lado c del triangulo : "));

		// Utilizamos los condicionales para saber el tip� de triangulo que es
		if (a == b && b == c) {
			// Encontramos el triangulo equilatero y mostramos el mensaje en pantalla
			JOptionPane.showMessageDialog(null, "El triangulo es de tipo equilatero");
		} else if (a == b || b == c) {
			// Encontramos el triangulo is�sceles y mostramos el mensaje en pantalla
			JOptionPane.showMessageDialog(null, "El triangulo es is�sceles:");
		}
		if (a != b || b != c) {
			// Encontramos el triangulo escaleano y mostramos el mensaje en pantalla
			JOptionPane.showMessageDialog(null, "El triangulo es  escaleno");
		}
	}
}